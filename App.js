import React from 'react';
import { StyleSheet, View } from 'react-native';

//Redux
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from './src/reducers';
const store = createStore(rootReducer);

//Routes
import { NativeRouter, Route, Link } from 'react-router-native';

//Components
import Welcome from './src/containers/Welcome';
import NavBar from './src/components/NavBar';

export default function App() {
  return (
    <Provider store={store}>
      <NativeRouter>
        <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
          <NavBar />
          <View>
            <Route exact path="/" component={Welcome}/>
          </View>
        </View>
      </NativeRouter>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    color: 'red'
  }
});
